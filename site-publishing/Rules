#!/usr/bin/env ruby

preprocess do
  create_hardware_list(@items)

  mds = @items.find_all { |i| i.identifier.ext == 'md' }
  mds.each { |item| create_toc_and_title_from_headings(item) }

  @config[:view_types] = {
    'index.html' => 'combined',
    'combined-alt.html' => 'combined alt',
    'bat-all.html' => 'BAT all',
    'shards-all.html' => 'shards all'
  }

  @config[:trees] = %w(
    drm-tip
    drm-intel-fixes
    drm-intel-next-fixes
    drm-intel-next
    drm-intel-gt-next
    drm-misc-fixes
    drm-misc-next-fixes
    drm-intel-media
    linus
    linux-next
    drm-next
    intel-xe
  )

  @config[:trees].each {|tree| create_tree(tree)}
  create_pre_merge_templates('drm-tip')
  create_post_merge_templates('drm-tip')
end

layout '/**/*.erb', :erb

compile '/**/*.md' do
  filter :kramdown, syntax_highlighter: :rouge
  layout '/docs.*'
  filter :md_links_to_html
  write ext: 'html'
end

compile '/**/filelist.html' do
  layout '/filelist.*'
  write ext: 'html'
end

compile '/tree/**/*.html' do
  layout '/grids.*'
  write ext: 'html'
end

compile '/test_result.js.erb' do
  filter :erb
  write '/assets/test_result.js'
end

compile '/hardware/*/*.txt' do
  write item.identifier.to_s
end

compile '/*.{css,gif,js}' do
  write '/assets' + item.identifier
end

passthrough '/*.yml'

ignore '/LICENSE'

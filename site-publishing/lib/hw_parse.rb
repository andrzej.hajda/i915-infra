#!/usr/bin/env ruby
require 'yaml'

GEN = { # fractions, so we have per-codename ordering
        "gd"  => 3.0,  # Grantsdale
        "gdg" => 3.0,  # Grantsdale-G
        "blb" => 3.5,  # Bear Lake-B
        "pnv" => 3.6,  # Pine View
        "bw"  => 4.0,  # Broadwater
        "bwr" => 4.0,  # Broadwater
        "cl"  => 4.2,  # Crestline
        "elk" => 4.5,  # Eagle Lake
        "ctg" => 4.6,  # Cantiga
        "ilk" => 5.0,  # Iron Lake
        "snb" => 6.0,  # Sandy Bridge
        "ivb" => 7.0,  # Ivy Bridge
        "byt" => 7.1,  # Bay Trail
        "svm" => 7.2,  # Silvermont
        "hsw" => 7.5,  # Haswell
        "bdw" => 8.0,  # Broadwell
        "bsw" => 8.5,  # Braswell
        "skl" => 9.0,  # Skylake
        "bxt" => 9.1,  # Broxton
        "apl" => 9.1,  # Apollo Lake
        "kbl" => 9.5,  # Kaby Lake
        "glk" => 9.6,  # Gemini Lake
        "cfl" => 9.7,  # Coffee Lake
        "whl" => 9.8,  # Whiskey Lake
        "cml" => 9.9,  # Comet Lake
        "cnl" => 10.0, # Cannon Lake

        "iclb" => 11.0, # Ice Lake pre-prod
        "icl"  => 11.0, # Ice Lake
        "ehl"  => 11.5, # Elkhart Lake

        "tgl"  => 12.0, # Tiger Lake
        "dg1"  => 12.1, # Discrete Graphics 1
        "rkl"  => 12.2, # Rocket Lake
        "jsl"  => 12.3, # Jasper Lake
        "adl"  => 12.5, # Alder Lake
        "adls" => 12.5, # Alder Lake
        "adlp" => 12.5, # Alder Lake
        "adlm" => 12.5, # Alder Lake

        "dg2" => 13.1, # Intel Arc Alchemist
}

module EDID
  EDID_LENGTH = 128

  def self.get_manufacturer(raw_edid)
    # big endian, 16 bits
    number = raw_edid[8,2].unpack1('S>')

    # 3 characters using 5 bits each
    [(number >> 10) & 0x1F, (number >> 5) & 0x1F, number & 0x1F]
      .map { |x| x + 'A'.ord - 1 } # let's shift them to uppercase ASCII
      .pack('CCC') # pack them back to a string
  end


  def self.get_ascii_descriptors(raw_edid)
    raw_edid[54..125].chars
      .each_slice(18) # each 18 byte descriptor field
      .to_a.map(&:join) # back to strings
      .select {| desc| [0xFC, 0xFE, 0xFF].include?(desc[3].ord) } # only ASCII ones
      .map { |desc| desc[5..-1].strip } # get contents and strip white space
      .compact
  end


  def self.parse_edid(dir, conn)
    raw_edid_path = File.join(dir, 'edid-' + conn)
    unless File.exists? raw_edid_path
      STDERR.puts "no edid for #{dir} #{conn}"
      return nil
    end
    raw_edid = File.read(raw_edid_path, mode: 'rb')

    if raw_edid.size < EDID_LENGTH
      STDERR.puts "too short edid for #{dir} #{conn}"
      return nil
    end

    [get_manufacturer(raw_edid), get_ascii_descriptors(raw_edid)]
      .flatten.join(' ')
  end
end


module I915DisplayInfo
  def self.is_connector_line?(line)
   line =~ /^\[?connector:? ?\d+/i
  end


  def self.is_dp_branch_line?(line)
   line =~ /DP branch device present:/
  end


  def self.is_display_port(name)
    name =~ /^DP-/
  end

  def self.get_branch_info(lines)
    type = lines.shift
    name = lines.shift
    hw = lines.shift
    sw = lines.shift
    [type, name, hw, sw].map { |x| x.split(': ')[1].strip }
  end


  def self.get_status_and_name(line)
      connector = line.scan(/\[?connector(?: |:)\d+:(?: type )?(\S+)?(?:]:|,) status: (connected|disconnected)/i)
      connector.flatten!
  end


  def self.parse(dir)
    display_info_path = File.join(dir, 'i915_display_info.txt')
    unless File.exists? display_info_path
      puts "no i915_display_info for #{dir}"
      return nil
    end

    lines = File.readlines(display_info_path)
    lines.shift until lines.empty? || lines.first =~ /^Connector info$/
    connector_status = {}

    loop do
      lines.shift while !(lines.empty? || is_connector_line?(lines.first))
      break if lines.empty?

      name, status = get_status_and_name(lines.shift)
      connector_status[name] = { 'status' => status }


      if is_display_port(name)
        lines.shift while !(lines.empty? || is_dp_branch_line?(lines.first) || is_connector_line?(lines.first))

        if is_dp_branch_line?(lines.first)
          branch = lines.shift.scan(/DP branch device present: (yes|no)/)
          if branch.flatten.first == 'yes'
            type, dev, hw, sw = get_branch_info(lines)
            connector_status[name]['to']  = "#{type}"       if status == 'connected'
            connector_status[name]['via'] = "#{dev} v#{sw}" if !dev.empty?
          end
        end
      end

      # TODO: full DP topology!

      if status == 'connected'
        display = EDID.parse_edid(dir, name)
        connector_status[name]['display'] = display if display
      end
    end

    connector_status
  end
end


module HWParse
  def self.cleanup_shards(machines)
    shards = {}

    machines.keys.grep(/^shard-/).each do |name|
      shard = name.sub(/\d+$/, '')
      shards[shard] ||= []
      shards[shard] << name
    end

    shards.keys.each do |key|
      machines[key] = machines[shards[key].last]
    end

    shards.each_pair do |shard, v|
      v.each do |machine|
        if machines[machine] != machines[shard]
          STDERR.puts "#{machine} is different from other shards!"
        end
      end
    end

    shards.values.flatten.each {|shard| machines.delete(shard)}

    machines
  end

  def self.parse_cpuinfo(dir)
    cpuinfo_path = File.join(dir, 'cpuinfo.log')
    unless File.exists? cpuinfo_path
      STDERR.puts "no cpuinfo for #{dir}"
      return nil
    end

    cpuinfo = File.read(cpuinfo_path)
    cpuinfo.scan(/model name\s+:(.*)/).flatten.first.strip
  end


  def self.get_gen(name)
    GEN[name.split(/-|(\d+$)/)[1]]
  end

  def self.get_filelist(dir)
    filelist = {}

    Dir.chdir(dir) do
      (Dir.entries('.') - ['.', '..']).each do |file|
        filelist[file] = File.read(file)
      end
    end

      filelist
  end


  def self.parse(dir)
    machines = {}

    unless Dir.exist?(dir)
      warn("WARNING: #{dir} directory does not exist, hardware list will be empty")
      return {}
    end

    Dir.chdir(dir) do
      (Dir.entries('.') - ['.', '..']).each do |child|
        next if not File.directory?(child)
        cpu = parse_cpuinfo(child)
        gen = get_gen(child).to_i
        filelist = get_filelist(child)
        display = I915DisplayInfo.parse(child)

        machines[child] = { 'cpu' => cpu,
                            'gen' => gen,
                            'display' =>  display,
                            'filelist' => filelist }
      end
    end

    cleanup_shards(machines)

    machines.sort do |x, y|
      x = x.first
      y = y.first
      gen_cmp = -(get_gen(x) <=> get_gen(y)) # reversed, so newest first
      name_cmp = -(x <=> y)                  # reversed lexicographic

      # sort by gen/codename first, then by name
      gen_cmp != 0 ? gen_cmp : name_cmp
    end.to_h
  end
end

if __FILE__ == $0
  puts HWParse.parse(ARGV[0]).to_yaml
end

#!/usr/bin/env python3
#
# Example .netrc:
#
# machine patchwork.freedesktop.org
#  login Friendly_CI_BOT
#  password EXTERMINATE
#
# Example invocations:
#
# ./patchwork_post.py -s success -n Fi.CI.BAT series 1234 1
# echo WOOOOOOO | ./patchwork_post.py -s warning -n Fi.CI.IGT -f - patch 3456

from urllib.parse import urljoin

import requests
import argparse
import sys

STATES = ["success", "warning", "failure", "pending", "info"]
FDO_PATCHWORK_URL = "https://patchwork.freedesktop.org/api/1.0/"


def build_result(test_name, state, summary=None, url=None):
    assert state in STATES
    result = {"test_name": test_name, "state": state}

    if summary:
        result["summary"] = summary

    if url:
        results["url"] = url

    return result


def post_result(result, base_url, patch_id=None, series_id=None, revision=None):
    if patch_id:
        assert series_id is None and revision is None
        url_path = "patches/{}/test-results/".format(patch_id)
    elif series_id and revision:
        assert patch_id is None
        url_path = "series/{}/revision/{}/test-results/".format(series_id, revision)
    else:
        assert 0  # shouldn't happen

    url = urljoin(base_url, url_path)
    res = requests.post(url, data=result)
    res.raise_for_status()


def read_summary(summary_path):
    if summary_path is None:
        return None

    if summary_path == "-":
        summary_file = sys.stdin
    else:
        summary_file = open(summary_path, "r")

    try:
        summary = summary_file.read()
    except IOError as e:
        print("Error reading {}: {}".format(summary_path, e))
        exit(1)
    finally:
        summary_file.close()

    return summary


def handle_args():
    ap = argparse.ArgumentParser(
        description="Post test results to Patchwork. Use .netrc for credentials."
    )
    sp = ap.add_subparsers(dest="subparser_name", required=True)

    pp = sp.add_parser("patch", help="patch_id")
    pp.add_argument("patch_id", type=int)

    ss = sp.add_parser("series", help="series_id revision")
    ss.add_argument("series_id", type=int)
    ss.add_argument("revision", type=int)

    ap.add_argument("-s", "--test-state", required=True, choices=STATES)
    ap.add_argument(
        "-n",
        "--test-name",
        required=True,
        help="Name of the test as defined in patchwork (e.g. 'Fi.CI.IGT').",
    )
    ap.add_argument("-u", "--url", help="Link to the detailed results.")
    ap.add_argument(
        "-f",
        "--summary-file",
        help="File with the summary of the results that will be send as an email. Use '-' for stdin.",
    )
    ap.add_argument(
        "-b",
        "--base-url",
        default=FDO_PATCHWORK_URL,
        help="Base URL of Patchwork's API, defaults to {}".format(FDO_PATCHWORK_URL),
    )

    return ap.parse_args()


if __name__ == "__main__":
    args = handle_args()
    summary = read_summary(args.summary_file)
    result = build_result(
        test_name=args.test_name, state=args.test_state, summary=summary, url=args.url
    )

    if args.subparser_name == "patch":
        post_result(result, args.base_url, patch_id=args.patch_id)
    elif args.subparser_name == "series":
        post_result(
            result, args.base_url, series_id=args.series_id, revision=args.revision
        )
    else:
        assert 0  # shouldn't happen

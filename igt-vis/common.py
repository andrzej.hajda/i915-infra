from html import escape
import re


def htmlname(testname):
    """Escape testname to filename-fitting format"""
    return re.sub(r"[ /|&?*]", "_", testname) + ".html"


def ttip(node):
    """
    Create tooltip for single test
    node = jsons[(build, host)]['tests'][test]
    """
    try:
        res = node["result"]
    except:
        res = "notrun"

    tooltip = ""

    if res == "fail" or res == "warn" or res == "crash":
        if "err" in node and node["err"]:
            tooltip = node["err"]
        elif "out" in node and node["out"]:
            tooltip = node["out"]
    elif (
        res == "dmesg-warn"
        or res == "dmesg-fail"
        or res == "timeout"
        or res == "incomplete"
        or res == "abort"
    ):
        if "dmesg-warnings" in node:
            tooltip = node["dmesg-warnings"]
        elif "dmesg" in node:
            tooltip = node["dmesg"]
        elif "out" in node:
            tooltip = node["out"]
    elif res == "skip":
        if "out" in node:
            tooltip = node["out"]

    if not tooltip:
        tooltip = ""

    return res, tooltip


def htmlttip(original, x=80, y=18):
    """
    Shorten text tooltip to manageable sized html
    limited by x * y characters box
    """
    s = original.split(sep="\n")
    for i, line in enumerate(s):
        if len(line) > x:
            s[i] = line[: x - 2] + "..."
    if len(s) < y:
        return escape("\n".join(s))
    s = "\n".join(s[: y - 1]) + "\n..."
    return escape(s)


def boxedletter(node):
    """Conditionally add symbol inside result cell in html"""
    letters = []

    if "dmesg-warnings" in node:
        letters.append("D")

    if "result" in node and node["result"] == "timeout":
        letters.append("T")

    if "aborted" in node:
        letters.append("A")

    if letters:
        return " ".join(letters)
    else:
        # Symbol Name: Figure Space
        return "&#8199;"

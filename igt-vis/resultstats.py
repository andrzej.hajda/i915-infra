#!/usr/bin/env python3
#
# resultstats.py
#
# Collect a bunch of statistics from results.json files
#
# Originally by Tomi Sarvela for Intel OTC lab
# Edited by Mikolaj Nowicki

import math
import re

from visutils import *

aborted_id = "igt@runner@aborted"


class Row:
    def __init__(self, j):
        parsed = self.parse_stats(j)
        self.good = parsed["good"]
        self.bad = parsed["bad"]
        self.notrun = parsed["notrun"]
        self.all = parsed["all"]
        self.passrate = parsed["passrate"]
        if "aborted_at" in parsed:
            self.aborted_at = parsed["aborted_at"]

    def parse_stats(self, j):
        dict = {}
        # Find if the run was aborted
        tests = [test for test in j["tests"]]
        if aborted_id in tests:
            tests.remove(aborted_id)
            dict["aborted_at"] = re.match(
                r".*\nPrevious test: (.*)\n", j["tests"]["igt@runner@aborted"]["out"]
            )[1]
            if dict["aborted_at"] == "nothing":
                dict["aborted_at"] = "boot"

        results = {
            "pass": 0,
            "skip": 0,
            "warn": 0,
            "fail": 0,
            "incomplete": 0,
            "dmesg-warn": 0,
            "dmesg-fail": 0,
            "crash": 0,
            "abort": 0,
            "timeout": 0,
            "notrun": 0,
        }
        for test in tests:
            results[j["tests"][test]["result"]] += 1

        dict["good"] = results["pass"] + results["skip"]
        dict["bad"] = (
            results["warn"]
            + results["fail"]
            + results["incomplete"]
            + results["dmesg-warn"]
            + results["dmesg-fail"]
            + results["crash"]
            + results["abort"]
            + results["timeout"]
        )
        # notrun is unknown, not fail
        dict["notrun"] = results["notrun"]
        # all should match good + bad + notrun
        dict["all"] = len(tests)
        if dict["good"] + dict["bad"] + dict["notrun"] != dict["all"]:
            print("WARNING: all doesn't match")
        dict["passrate"] = (
            math.floor(1000 * float(dict["good"]) / float(dict["all"])) / 10
        )
        return dict

# i915 Infra Kernel Configs

## debug

Default kernel config for CI purposes with a lot of debug options turned on.
Used as GCOV config with CONFIG_GCOV_KERNEL on.

## debug-kasan

Same as debug kernel but with KASAN enabled.


